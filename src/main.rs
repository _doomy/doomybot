use std::fs::File;
use std::io::Write;

use bevy::asset::LoadState;
use bevy::core::FixedTimestep;
use bevy::utils::Instant;
use bevy::{app::App, app::ScheduleRunnerSettings, prelude::*, utils::Duration};
use bevy::{
    asset::{AssetLoader, LoadContext, LoadedAsset},
    prelude::*,
    reflect::TypeUuid,
    utils::BoxedFuture,
};
use rand::prelude::IteratorRandom;
use rumblr::{Keys, LegacyPostOptionalParams, PostAction, PostType, TumblrClient};
use rust_bert::pipelines::common::ModelType;
use rust_bert::pipelines::question_answering::{QaInput, QuestionAnsweringModel};
use rust_bert::pipelines::text_generation::{TextGenerationConfig, TextGenerationModel};
use serde::Deserialize;
// This example only enables a minimal set of plugins required for bevy to run.
// You can also completely remove rendering / windowing Plugin code from bevy
// by making your import look like this in your Cargo.toml
//
// [dependencies]
// bevy = { version = "*", default-features = false }
// # replace "*" with the most recent version of bevy
use rand::seq::SliceRandom;
fn main() {
    App::build()
        .add_plugins(MinimalPlugins)
        .add_plugin(bevy::asset::AssetPlugin::default())
        .add_state(GameState::Load)
        .init_asset_loader::<SettingsAssetLoader>()
        .add_asset::<Settings>()
        .init_asset_loader::<TextAssetLoader>()
        .add_asset::<TextAsset>()
        .insert_non_send_resource(QuestionAnsweringModel::new(Default::default()).unwrap())
        .insert_non_send_resource(TextGenerationModel::new(Default::default()).unwrap())
        .init_resource::<PreloadingAssets>()
        .insert_resource(TumblrClient::new())
        .init_resource::<Timestamps>()
        .init_resource::<Handle<Settings>>()
        .init_resource::<Handle<TextAsset>>()
        .init_resource::<Vec<Ask>>()
        .insert_resource(Vec::<Ask>::default())
        .add_system_set(SystemSet::on_enter(GameState::Load).with_system(preload.system()))
        .add_system_set(
            SystemSet::on_update(GameState::Load)
                .with_system(transition_when_preloaded_system.system()),
        )
        .add_system_set(SystemSet::on_enter(GameState::Main).with_system(startup.system()))
        .add_system_set(
            SystemSet::on_update(GameState::Main)
                .with_system(answer_asks.system())
                .after("check"),
        )
        .add_system_set(
            SystemSet::on_update(GameState::Main)
                .with_system(check_asks.system())
                .label("check"),
        )
        .run();
}

/// Load files, assets, etc.
fn preload(
    assets: Res<AssetServer>,
    mut settings_handle: ResMut<Handle<Settings>>,
    mut corpus_handle: ResMut<Handle<TextAsset>>,
    mut loading: ResMut<PreloadingAssets>,
) {
    // load ron file settings
    *settings_handle = assets.load::<Settings, &'static str>("settings.settings");
    loading.0.push(settings_handle.clone_untyped());
    assets
        .watch_for_changes()
        .expect("could not watch for changes");

    // load corpus
    *corpus_handle = assets.load::<TextAsset, &'static str>("corpus.txt");
    loading.0.push(corpus_handle.clone_untyped());
    assets
        .watch_for_changes()
        .expect("could not watch for changes");
}

fn startup(
    mut client: ResMut<TumblrClient>,
    settings: Res<Assets<Settings>>,
    settings_handle: Res<Handle<Settings>>,
) {
    let settings = settings.get(settings_handle.clone()).unwrap();

    *client = TumblrClient::from_keys(Keys {
        consumer_key: settings.auth.key.clone(),
        consumer_secret: settings.auth.secret.clone(),
        access_key: settings.auth.token.clone(),
        access_secret: settings.auth.token_secret.clone(),
    });
}

fn check_asks(
    client: ResMut<TumblrClient>,
    mut asks: ResMut<Vec<Ask>>,
    settings: Res<Assets<Settings>>,
    settings_handle: Res<Handle<Settings>>,
    mut timestamps: ResMut<Timestamps>,
) {
    let settings = settings.get(settings_handle.clone()).unwrap();

    // whether or not we should check for asks
    let get_asks = match timestamps.last_checked_asks {
        Some(i) => {
            Instant::now().duration_since(i)
                >= Duration::from_millis(settings.intervals.check_asks as u64)
        }
        None => {
            timestamps.last_checked_asks = Some(Instant::now());
            true
        }
    };

    if get_asks {
        let asks_json = client.get_blog_posts_submission("doomybot", None);
        let posts_array = asks_json["response"]["posts"].as_array().unwrap().clone();
        // convert the dynamic value into a more reasonable struct -
        // also, generate a response
        // Rewrite the asks resource to keep it stateless or whatever
        asks.clear();
        *asks = posts_array
            .iter()
            .map(|ask| {
                let question = ask["summary"].as_str().unwrap();
                Ask {
                    question: question.into(),
                    author: ask["asking_name"].as_str().unwrap().into(),
                    answer: ask["answer"].as_str().unwrap().into(),
                    id: Some(ask["id"].as_u64().unwrap().to_string()),
                    response: None,
                }
            })
            .collect();

        timestamps.last_checked_asks = Some(Instant::now());
    }
}

fn answer_asks(
    settings: Res<Assets<Settings>>,
    settings_handle: Res<Handle<Settings>>,
    mut client: ResMut<TumblrClient>,
    mut qa_model: NonSend<QuestionAnsweringModel>,
    mut gen_model: NonSendMut<TextGenerationModel>,
    mut timestamps: ResMut<Timestamps>,
    mut asks: ResMut<Vec<Ask>>,
    mut corpus_handle: ResMut<Handle<TextAsset>>,
    mut corpus: ResMut<Assets<TextAsset>>,
) {
    let settings = settings.get(settings_handle.clone()).unwrap();

    // whether or not we should respond to an ask
    let respond = match timestamps.last_posted {
        Some(i) => {
            Instant::now().duration_since(i)
                >= Duration::from_millis(settings.intervals.answer_asks as u64)
        }
        None => true,
    };

    if respond {
        let mut success = false;
        if let Some(mut ask) = asks.iter_mut().choose(&mut rand::thread_rng()) {
            // actually respond to the ask
            let response = qa_model.predict(
                &[QaInput {
                    question: ask.clone().question.into(),
                    // TODO: set context to the entirety of our asks
                    context: corpus.get(corpus_handle.clone()).unwrap().0.clone(),
                }],
                1,
                32,
            );
            if let Some(r) = response.first() {
                if let Some(r) = r.first() {
                    // set to our answer if successful
                    ask.answer = r.answer.clone();
                    // now, generate some more text from this seed answer
                    ask.answer = gen_model.generate(&[ask.answer.as_str()], None).join(". ");

                    // set the gen model
                    *gen_model = TextGenerationModel::new(TextGenerationConfig {
                        model_type: ModelType::GPT2,
                        max_length: settings.generate.max_length,
                        num_beams: settings.generate.num_beams,
                        temperature: settings.generate.temperature,
                        num_return_sequences: settings.generate.num_return_sequences,
                        ..Default::default()
                    })
                    .unwrap();
                    // at this point, we also want to add the ask to our corpus and save the asset
                    if let Some(corpus) = corpus.get_mut(corpus_handle.clone()) {
                        corpus.0 += &ask.question;
                        let mut file = File::create("assets/corpus.txt").unwrap();
                        writeln!(file, "{}", corpus.0).unwrap();
                    }
                    // one more thing - we generate a little more than we need bc for whatever reason it likes to trail.
                    // so if it's over a certain amout we can cut off the rest
                    if ask.answer.chars().count() > 20 {
                        let arr = ask.answer.split(|c: char| c == '\n' || c == '.');
                        let mut arr: Vec<&str> = arr.collect();
                        arr.truncate(arr.len() - 1);
                        ask.answer = arr.join(" ");
                    }
                    success = true;
                }
            }
            if success {
                // Post to tumblr
                client.legacy_post(
                    &settings.blog,
                    PostAction::Edit(&ask.id.clone().unwrap()),
                    PostType::Answer {
                        answer: Some(&ask.answer),
                    },
                    Some(LegacyPostOptionalParams {
                        state: Some("published"),
                        ..Default::default()
                    }),
                );
                timestamps.last_posted = Some(Instant::now());
            }
        }

        //
    }
}

#[derive(Default, Debug, Clone)]
pub struct Ask {
    pub question: String,
    pub author: String,
    pub answer: String,
    pub id: Option<String>,
    pub response: Option<String>,
}

#[derive(Default, Debug, Clone, TypeUuid, Deserialize)]
#[uuid = "39cadc56-aa9c-4543-8640-a018b74b505a"]
pub struct Settings {
    pub auth: Auth,
    pub intervals: Intervals,
    pub blog: String,
    pub generate: GenerateSettings,
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct GenerateSettings {
    pub max_length: i64,
    pub num_beams: i64,
    pub temperature: f64,
    pub num_return_sequences: i64,
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct Auth {
    pub key: String,
    pub secret: String,
    pub token: String,
    pub token_secret: String,
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct Intervals {
    check_asks: usize,
    answer_asks: usize,
}

#[derive(Default)]
pub struct SettingsAssetLoader;

impl AssetLoader for SettingsAssetLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<(), anyhow::Error>> {
        Box::pin(async move {
            let settings = ron::de::from_bytes::<Settings>(bytes)?;
            load_context.set_default_asset(LoadedAsset::new(settings));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["settings"]
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum GameState {
    Load,
    Main,
}

impl Default for GameState {
    fn default() -> Self {
        Self::Load
    }
}

#[derive(Default)]
pub struct PreloadingAssets(pub Vec<HandleUntyped>);

pub fn transition_when_preloaded_system(
    loading: Res<PreloadingAssets>,
    mut state: ResMut<State<GameState>>,
    assets: Res<AssetServer>,
) {
    if loading
        .0
        .iter()
        .filter(|h| assets.get_load_state(*h) == LoadState::Loading)
        .count()
        == 0
    {
        // We are done loading! transition state.
        state.set(GameState::Main).ok();
    }
}

/// Keeps track of the last time things happened
#[derive(Default, Debug)]
pub struct Timestamps {
    pub last_checked_asks: Option<Instant>,
    pub last_posted: Option<Instant>,
}

#[derive(Default)]
pub struct TextAssetLoader;

impl AssetLoader for TextAssetLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<(), anyhow::Error>> {
        Box::pin(async move {
            let text = String::from_utf8(bytes.to_vec())?;
            load_context.set_default_asset(LoadedAsset::new(TextAsset(text)));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["txt"]
    }
}

#[derive(Default, Debug, Clone, TypeUuid, Deserialize)]
#[uuid = "39cadc56-aa9c-4543-8640-a018b74b505b"]
pub struct TextAsset(pub String);
